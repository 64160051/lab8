package com.thatchai.week8;

public class Circle {
    private String name;
    private double radian;
    public Circle(String name,int radian){
        this.name = name;
        this.radian = radian;
    }
    public int printCircleRadian(){
        double area = 3.14 * (radian*radian);
        System.out.println(name + "area = " + area);
        return (int)area;
    }
    public int printCirclePerimeter(){
        double perimeter = (2*3.14)*radian;
        System.out.println(name + "perimeter = " + perimeter); 
        return (int)perimeter;
    }
}
