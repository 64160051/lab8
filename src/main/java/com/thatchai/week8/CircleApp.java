package com.thatchai.week8;

public class CircleApp {
    public static void main(String[] args){
        Circle circle1 = new Circle("Circle1",1);
        circle1.printCircleRadian();
        Circle circle2 = new Circle("Circle2",2);
        circle2.printCircleRadian();
        circle1.printCirclePerimeter();
        circle2.printCirclePerimeter();
    }
}
