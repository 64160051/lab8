package com.thatchai.week8;

public class Rectangle {
    private String name;
    private int height;
    private int width;

    public Rectangle(String name,int height,int width){
        this.name = name;
        this.height = height;
        this.width = width;
    }
    public int printRectangleArea(){
        int area = height * width;
        System.out.println(name + "area = " + area);
        return area;
    }
    public int printRectanglePerimeter(){
        int perimeter = (height + width)*2;
        System.out.println(name + "perimeter = " + perimeter);
        return perimeter;
    }
}
